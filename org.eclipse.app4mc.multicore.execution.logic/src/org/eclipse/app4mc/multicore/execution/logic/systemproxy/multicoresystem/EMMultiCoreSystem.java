/**
 ********************************************************************************
 * Copyright (c) 2017 Dortmund University of Applied Sciences and Arts and others.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *    Dortmund University of Applied Sciences and Arts - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.multicore.execution.logic.systemproxy.multicoresystem;

import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import org.eclipse.app4mc.multicore.execution.logic.executionmodel.ExecutionModel;
import org.eclipse.app4mc.multicore.execution.logic.systemproxy.scheduler.ISchedulerAlgorithm;
import org.eclipse.app4mc.multicore.execution.logic.executionmodel.types.EMEventChain;
import org.eclipse.app4mc.multicore.execution.logic.executionmodel.types.EMEventChainHist;
import org.eclipse.app4mc.multicore.execution.logic.executionmodel.types.EMEventChainHist.TaskTime;
import org.eclipse.app4mc.multicore.execution.logic.executionmodel.types.EMEventChain.EventChainState;
import org.eclipse.app4mc.multicore.execution.logic.executionmodel.types.EMEventChainState;
import org.eclipse.app4mc.multicore.execution.logic.executionmodel.types.EMTask;
import org.eclipse.app4mc.multicore.execution.logic.executionmodel.types.EMTaskHistState;
import org.eclipse.app4mc.multicore.execution.logic.executionmodel.types.EMTaskState;

public class EMMultiCoreSystem extends MultiCoreSystem {

	private final ExecutionModel execModel;

	private final List<EMEventChain> eventChains = new LinkedList<>();

	private List<EMEventChainHist> eventChainsHist = new LinkedList<>();


	public EMMultiCoreSystem(long timeinc, Supplier<ISchedulerAlgorithm> schedulerSuplier, ExecutionModel m) {
		super(timeinc, schedulerSuplier);
		this.execModel = m;
	}

	@Override
	public void addCoreScheduler(String corename) {
		if (!getSchedulers().containsKey(corename)) {
			super.addCoreScheduler(corename);
			addListener(corename, new EMTracer(corename, execModel));
		}
	}

	@Override
	public void createEMEventChains() {
		// TODO Add event chains to execution model. Call method in executionModel
		// to build accordingly to the task history.
		//System.out.println("Create EM EVENT CHAINS IN EMMultiCore");

		List<EMTask> var = new LinkedList<EMTask>(execModel.getTasks().values());
		
		

		for (EMTask task : var) {
			for (EMEventChain evnCh : eventChains) {
				if (task.getName().equalsIgnoreCase(evnCh.getTaskName())
						&& evnCh.getState() == EMEventChainState.START) {
					for (EMTaskHistState taskHist : task.getStateHistory()) {
						if (taskHist.getState() == EMTaskState.RUNNING) {
							/*System.out.println("Start Chain");
							System.out.println(evnCh.getName());
							System.out.println(task.getName());
							System.out.println(taskHist.getStart());*/
							EMEventChainHist eventChainHist = new EMEventChainHist(evnCh.getName());
							eventChainHist.setStart(taskHist.getStart());
							eventChainHist.setStartTask(task.getName());
							eventChainHist.setStartEMTask(task);
							eventChainHist.setStartTaskHist(taskHist);
							eventChainHist.setRunnable(evnCh.getRunnableName());
							eventChainHist.setStartCore(taskHist.getCore());
							eventChainHist.setEventChainName(evnCh.getName());
							

							/*
							 * for (EMEventChain evnChEnd : eventChains) { if (evnChEnd.getState() ==
							 * EMEventChainState.END &&
							 * evnChEnd.getName().equalsIgnoreCase(evnCh.getName())) {
							 * System.out.println("ENDS"); System.out.println(evnChEnd.getTaskName()); } }
							 */
							eventChainsHist.add(eventChainHist);
						}
					}
				}
			}
		}
		
		for (EMEventChain eventChain : eventChains) {
			for (EMEventChainHist currEvChhist : eventChainsHist) {
				if (currEvChhist.getEventChainName().equalsIgnoreCase(eventChain.getName()) 
						&& !currEvChhist.getStartTask().equalsIgnoreCase(eventChain.getTaskName())) {
					execModel.addEventChain(eventChain);
				}
			}			
		}

		for (EMTask task : var) {
			for (EMEventChain evnCh : eventChains) {
				// Check if the taks in question makes part of an event chain and is the end of
				// a chain
				if (task.getName().equalsIgnoreCase(evnCh.getTaskName()) && evnCh.getState() == EMEventChainState.END) {

					// How to know if there's another task ahead
					for (EMTaskHistState taskHist : task.getStateHistory()) {
						if (taskHist.getState() == EMTaskState.RUNNING) {
							// Check the current list of event chains hist (partiall with starts only) to
							// validate if the event chain in examination is not intertask eventchain and
							// if this event happens after the start of the event chain
							for (EMEventChainHist currEvChhist : eventChainsHist) {
								if (currEvChhist.getEventChainName().equalsIgnoreCase(evnCh.getName())
										&& !currEvChhist.getStartTask().equalsIgnoreCase(task.getName())
										&& currEvChhist.getStartTaskHist().getEnd() <= taskHist.getStart()) {

									long startChainTaskDuration = currEvChhist.getStartTaskHist().getEnd()
											- currEvChhist.getStartTaskHist().getStart();

									long endTimeNextStartChainTask = currEvChhist.getStartEMTask().getPeriod()
											+ currEvChhist.getStartTaskHist().getStart();

									if ((startChainTaskDuration + endTimeNextStartChainTask) > taskHist.getStart()) {
										/*System.out.println(evnCh.getName());
										System.out.println(currEvChhist.getStartTask() + "->" + task.getName());
										System.out.println(currEvChhist.getStartTaskHist().getStart() + "->"
												+ taskHist.getStart());
										System.out.println(currEvChhist.getStartCore().getName() + "->"
												+ taskHist.getCore().getName());
										System.out.println("---------");
										System.out.println("");*/
										EMEventChainHist hec = new EMEventChainHist(evnCh.getName());
										hec.setStart(currEvChhist.getStartTaskHist().getStart());
										hec.setEnd(taskHist.getEnd());
										hec.setStartTask(currEvChhist.getStartTask());
										hec.setEndEMTask(task);
										hec.setStartEMTask(currEvChhist.getStartEMTask());
										hec.setEndTask(task.getName());
										hec.setStartCore(currEvChhist.getStartCore());
										hec.setEndCore(taskHist.getCore());
										hec.setStartTaskTimes(currEvChhist.getStartTaskHist().getStart(), currEvChhist.getStartTaskHist().getEnd());
										hec.setEndTaskTimes(taskHist.getStart(), taskHist.getEnd());
										execModel.addEventChainHistEntry(hec);
									}

								}
							}
						}
					}
				}
			}
		}

		// super.createEMEventChains();

	}

	@Override
	public void addEventChain(String eventChainName, String runnableName, String taskName, String coreName,
			EMEventChainState state) {
		
		EMEventChain eventChain = new EMEventChain(eventChainName, runnableName, taskName, state);
		eventChains.add(eventChain);
		// super.addEventChain(eventChainName, runnableName, taskName, coreName, state);
	}

}
