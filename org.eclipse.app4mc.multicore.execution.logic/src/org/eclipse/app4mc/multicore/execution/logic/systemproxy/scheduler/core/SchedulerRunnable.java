package org.eclipse.app4mc.multicore.execution.logic.systemproxy.scheduler.core;

import java.util.LinkedList;
import java.util.List;

public class SchedulerRunnable {

	private final String name;

	private List<SchedulerLabel> writeLabels = new LinkedList<>();;
	private List<SchedulerLabel> readLabels = new LinkedList<>();;

	public SchedulerRunnable(String name, List<SchedulerLabel> wLabels, List<SchedulerLabel> rLabels) {
		this.name = name;
		setWriteLabels(wLabels);
		setReadLabels(rLabels);
	}

	public String getName() {
		return name;
	}

	public List<SchedulerLabel> getWriteLabels() {
		return writeLabels;
	}

	public void setWriteLabels(List<SchedulerLabel> writeLabels) {
		this.writeLabels = writeLabels;
	}

	public List<SchedulerLabel> getReadLabels() {
		return readLabels;
	}

	public void setReadLabels(List<SchedulerLabel> readLabels) {
		this.readLabels = readLabels;
	}

}
