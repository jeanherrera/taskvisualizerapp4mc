package org.eclipse.app4mc.multicore.execution.logic.executionmodel.types;

public class EMLabel {
	
	private String name;
	
	private LabelAccess typeOfAccess;
	
	public EMLabel(String name, LabelAccess labelAccess)
	{
		this.setName(name);
		this.setTypeOfAccess(labelAccess);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public LabelAccess getTypeOfAccess() {
		return typeOfAccess;
	}

	public void setTypeOfAccess(LabelAccess typeOfAccess) {
		this.typeOfAccess = typeOfAccess;
	}

}
