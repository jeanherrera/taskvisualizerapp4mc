/**
 ********************************************************************************
 * Copyright (c) 2017 Dortmund University of Applied Sciences and Arts and others.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *    Dortmund University of Applied Sciences and Arts - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.multicore.execution.logic.executionmodel;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.function.BiConsumer;
import java.util.function.Consumer;

import org.eclipse.app4mc.multicore.execution.logic.executionmodel.misc.EMTimeType;
import org.eclipse.app4mc.multicore.execution.logic.executionmodel.types.EMCore;
import org.eclipse.app4mc.multicore.execution.logic.executionmodel.types.EMEventChain;
import org.eclipse.app4mc.multicore.execution.logic.executionmodel.types.EMEventChainHist;
import org.eclipse.app4mc.multicore.execution.logic.executionmodel.types.EMRunnable;
import org.eclipse.app4mc.multicore.execution.logic.executionmodel.types.EMTask;
import org.eclipse.app4mc.multicore.execution.logic.executionmodel.types.EMTaskHistState;
import org.eclipse.app4mc.multicore.execution.logic.executionmodel.types.EMTask.DeadlineEntry;

public class ExecutionModel {

	private final Map<String, EMCore> cores = new HashMap<>();

	private final List<Consumer<EMCore>> coreAddedListener = new LinkedList<>();

	private final Map<String, EMTask> tasks = new HashMap<>();
	
	private final Map<String, EMEventChain> eventChains = new HashMap<>();
	
	private final List<EMEventChainHist> eventChainEntries = new LinkedList<>();

	private final List<Consumer<EMTask>> taskAddedListener = new LinkedList<>();

	private final List<Consumer<EMRunnable>> runnableAddedListener = new LinkedList<>();

	private List<BiConsumer<EMTask, EMTaskHistState>> histEntryListener = new LinkedList<>();

	private List<BiConsumer<EMTask, DeadlineEntry>> deadlineListener = new LinkedList<>();
	
	private List<Consumer<EMEventChainHist>> eventChainHistListener = new LinkedList<>();
	
	private List<Consumer<EMEventChain>> eventChainListener = new LinkedList<>();

	private EMTimeType timeScale = EMTimeType.NONE;

	public Map<String, EMCore> getCores() {
		return cores;
	}

	/**
	 * Tasks in of after execution.
	 * 
	 * @see EMTask
	 * @return
	 */
	public Map<String, EMTask> getTasks() {
		return tasks;
	}

	public void setTimeScale(EMTimeType t) {
		this.timeScale = t;
	}

	public EMTimeType getTimeScale() {
		return timeScale;
	}

	public void addTaskDeadlineMissedEntry(String taskName, String core, Long time) {
		final EMTask t = tasks.get(taskName);
		DeadlineEntry d = new DeadlineEntry(time, cores.get(core));
		t.getMissedDeadlines().add(d);
		notifyTaskDeadlineMissed(t, d);
	}

	/**
	 * Add a history entry to a task (e.g state running from time point 1000 to
	 * 1223). By adding the entry with this method observer will be notified about
	 * completed task hist entries.
	 * 
	 * @param taskName
	 * @param e
	 */
	public void addTaskHistEntry(String taskName, EMTaskHistState e) {
		final EMTask t = tasks.get(taskName);
		t.getStateHistory().add(e);
		notifyTaskHistEntryListener(t, e);
	}
	
	public void addEventChainHistEntry(EMEventChainHist hec) {
		getEventChainsEntries().add(hec);
		notifyEventChainHistEntryListener(hec);
	}
	
	public void addEventChain(EMEventChain eventChain) {
		if(eventChains.containsKey(eventChain.getName())){
			return;
		} else {
			getEventChains().put(eventChain.getName(), eventChain);
			notifyEventChainListener(eventChain);
		}
	}

	/**
	 * Add a task to the model. Only this method will notify observers about added
	 * tasks.
	 * 
	 * @param t
	 */
	public synchronized void addTask(EMTask t) {
		tasks.put(t.getName(), t);
		notifyTaskAddedListener(t);
	}

	public synchronized void addRunnable(String task, EMRunnable r) {
		tasks.get(task).getRunnableSequence().add(r);		
		notifyRunnableAddedListener(r);
	}
	
	

	/**
	 * Add a core to the model. Only this method will notify observers about added
	 * cores.
	 * 
	 * @param c
	 */
	public void addCore(EMCore c) {
		cores.put(c.getName(), c);
		notifyAllCoreAddedObserver(c);
	}

	private void notifyAllCoreAddedObserver(EMCore c) {
		coreAddedListener.forEach(x -> x.accept(c));
	}

	private void notifyTaskAddedListener(EMTask t) {
		taskAddedListener.forEach(x -> x.accept(t));
	}

	private void notifyRunnableAddedListener(EMRunnable r) {
		runnableAddedListener.forEach(x -> x.accept(r));
	}
	
	public void addRunnableAddedListener(Consumer<EMRunnable> t) {
		runnableAddedListener.add(t);
	}

	private void notifyTaskHistEntryListener(EMTask t, EMTaskHistState e) {
		histEntryListener.forEach(x -> x.accept(t, e));
	}
	
	private void notifyEventChainListener(EMEventChain eventChain) {
		eventChainListener.forEach(x -> x.accept(eventChain));
	}
	
	private void notifyEventChainHistEntryListener(EMEventChainHist hec) {
		eventChainHistListener.forEach(x -> x.accept(hec));
	}

	public void addTaskHistEntryAddedListener(BiConsumer<EMTask, EMTaskHistState> e) {
		histEntryListener.add(e);
	}

	public void addTaskAddedListener(Consumer<EMTask> t) {
		taskAddedListener.add(t);
	}

	public void addCoreAddedListener(Consumer<EMCore> c) {
		coreAddedListener.add(c);
	}

	public void addTaskDeadlineListener(BiConsumer<EMTask, DeadlineEntry> e) {
		deadlineListener.add(e);
	}
	
	public void addEventChainHistListener(Consumer<EMEventChainHist> e) {
		eventChainHistListener.add(e);
	}
	
	public void addEventChainListener(Consumer<EMEventChain> e) {
		eventChainListener.add(e);
	}

	private void notifyTaskDeadlineMissed(EMTask t, DeadlineEntry e) {
		deadlineListener.forEach(x -> x.accept(t, e));
	}

	/**
	 * @return the eventChains
	 */
	public List<EMEventChainHist> getEventChainsEntries() {
		return eventChainEntries;
	}

	/**
	 * @return the eventChains
	 */
	public Map<String, EMEventChain> getEventChains() {
		return eventChains;
	}

}
