package org.eclipse.app4mc.multicore.execution.logic.executionmodel.types;

public enum LabelAccess {
	WRITE,
	READ
}
