/**
 ********************************************************************************
 * Copyright (c) 2017 Dortmund University of Applied Sciences and Arts and others.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *    Dortmund University of Applied Sciences and Arts - initial API and implementation
 *******************************************************************************/

package org.eclipse.app4mc.multicore.execution.logic.systemproxy;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.function.Supplier;

import org.eclipse.app4mc.amalthea.model.ConstraintsModel;
import org.eclipse.app4mc.amalthea.model.Event;
import org.eclipse.app4mc.amalthea.model.EventChain;
import org.eclipse.app4mc.amalthea.model.EventChainItem;
import org.eclipse.app4mc.amalthea.model.Label;
import org.eclipse.app4mc.amalthea.model.Process;
import org.eclipse.app4mc.amalthea.model.Runnable;
import org.eclipse.app4mc.amalthea.model.RunnableEvent;
import org.eclipse.app4mc.amalthea.model.util.DeploymentUtil;
import org.eclipse.app4mc.amalthea.model.util.SoftwareUtil;
import org.eclipse.app4mc.multicore.execution.logic.executionmodel.ExecutionModel;
import org.eclipse.app4mc.multicore.execution.logic.executionmodel.types.EMEventChainState;
import org.eclipse.app4mc.multicore.execution.logic.executionmodel.types.EMLabel;
import org.eclipse.app4mc.multicore.execution.logic.openmapping.MalformedModelException;
import org.eclipse.app4mc.multicore.execution.logic.openmapping.OMAllocation;
import org.eclipse.app4mc.multicore.execution.logic.openmapping.OMMapping;
import org.eclipse.app4mc.multicore.execution.logic.openmapping.OMRunnable;
import org.eclipse.app4mc.multicore.execution.logic.systemproxy.multicoresystem.EMMultiCoreSystem;
import org.eclipse.app4mc.multicore.execution.logic.systemproxy.multicoresystem.MultiCoreSystem;
import org.eclipse.app4mc.multicore.execution.logic.systemproxy.multicoresystem.SimUtil;
import org.eclipse.app4mc.multicore.execution.logic.systemproxy.scheduler.ISchedulerAlgorithm;
import org.eclipse.app4mc.multicore.execution.logic.systemproxy.scheduler.IStepScheduler;
import org.eclipse.app4mc.multicore.execution.logic.systemproxy.scheduler.core.SchedulerLabel;
import org.eclipse.emf.common.util.EList;

public class SystemProxyFactory {

	public static ISystemProxy createSystemProxy(long timeScale, Supplier<ISchedulerAlgorithm> schedulerAlgoFactory) {
		return new MultiCoreSystem(timeScale, schedulerAlgoFactory);
	}

	/**
	 * Create system proxy which traces to the provided ExecutionModel. Already
	 * added content from the provided ExecutionModel will not be considered.
	 * Therefore it's advised to pass an empty model.
	 * 
	 * @param timescale
	 * @param schedulerAlgoFactory
	 * @param m
	 * @return
	 */
	public static ISystemProxy createEMSystemProxy(long timescale, Supplier<ISchedulerAlgorithm> schedulerAlgoFactory,
			ExecutionModel m) {
		return new EMMultiCoreSystem(timescale, schedulerAlgoFactory, m);
	}

	public static ISystemProxy createEMSystemProxyFromOMMapping(long timescale,
			Supplier<ISchedulerAlgorithm> schedulerAlgoFactory, OMMapping omMapping, ExecutionModel m,
			ConstraintsModel cm) throws SimException {

		Map<String, String> runnablesCores = new HashMap<>();

		SimUtil.validateModelForSimulation(omMapping);

		ISystemProxy sim = createEMSystemProxy(timescale, schedulerAlgoFactory, m);

		EList<EventChain> modelEventChains = cm.getEventChains();

		for (OMAllocation a : omMapping.getAllocationList()) {
			try {
				sim.addTask(a.getCore().getCoreRef().getName(), a.getTask().getTaskRef().getName(),
						a.calculateProcessingTime(), a.getTask().getPeriod());
				for (OMRunnable r : a.getTask().getRunnableCallSequence()) {

					Set<Label> readLabels = SoftwareUtil.getReadLabelSet(r.getRunnableRef(), null);
					Set<Label> writeLabels = SoftwareUtil.getWriteLabelSet(r.getRunnableRef(), null);

					List<SchedulerLabel> wrLabels = new LinkedList<>();
					List<SchedulerLabel> rdLabels = new LinkedList<>();

					for (Label lb : writeLabels) {
						SchedulerLabel label = new SchedulerLabel(lb.getName());
						wrLabels.add(label);
					}

					for (Label lb : readLabels) {
						SchedulerLabel label = new SchedulerLabel(lb.getName());
						rdLabels.add(label);
					}
					runnablesCores.put(r.getRunnableRef().getName(), a.getCore().getCoreRef().getName());

					sim.addRunnable(a.getCore().getCoreRef().getName(), a.getTask().getTaskRef().getName(),
							r.getRunnableRef().getName(), wrLabels, rdLabels);
				}
			} catch (MalformedModelException e) {
				throw new SimException(e.getMessage());
			}
		}

		for (EventChain evnChain : modelEventChains) {

			System.out.println(evnChain.getName());
			System.out.println(evnChain.getStimulus().getName());
			System.out.println(evnChain.getResponse().getName());
			System.out.println("------------------------------");

			for (EventChainItem chainItem : evnChain.getSegments()) {
				String eventChainName = chainItem.getEventChain().getName();

				System.out.println(eventChainName);

				Event stimulusEvent = chainItem.getEventChain().getStimulus();

				if (stimulusEvent instanceof RunnableEvent) {
					Runnable runn = ((RunnableEvent) stimulusEvent).getEntity();
					String runnableName = runn.getName();
					List<Process> processes = SoftwareUtil.getProcesses(runn, null);
					String taskName = processes.get(0).getName();
					System.out.println(runnableName);
					System.out.println(taskName);
					String coreName = runnablesCores.get(runnableName);
					System.out.println("Core:  " + coreName);
					sim.addEventChain(eventChainName, runnableName, taskName, coreName, EMEventChainState.START);
				}
				// else break;

				Event responseEvent = chainItem.getEventChain().getResponse();

				if (responseEvent instanceof RunnableEvent) {
					Runnable runn = ((RunnableEvent) responseEvent).getEntity();
					String runnableName = runn.getName();
					List<Process> processes = SoftwareUtil.getProcesses(runn, null);
					String taskName = processes.get(0).getName();
					System.out.println(runnableName);
					System.out.println(taskName);
					String coreName = runnablesCores.get(runnableName);
					System.out.println("Core:  " + coreName);
					sim.addEventChain(eventChainName, runnableName, taskName, coreName, EMEventChainState.END);
				}
			}
			System.out.println("////////////////");
			System.out.println("");
		}

		return sim;
	}

}
