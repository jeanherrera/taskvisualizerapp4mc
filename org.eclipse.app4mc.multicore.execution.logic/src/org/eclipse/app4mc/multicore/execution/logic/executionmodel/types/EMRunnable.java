package org.eclipse.app4mc.multicore.execution.logic.executionmodel.types;

import java.util.LinkedList;
import java.util.List;

public class EMRunnable {

	private String name;
	
	private String parentTaskName;

	private List<EMLabel> labels= new LinkedList<EMLabel>();

	public EMRunnable(String name, String parentTask) {
		this.name = name;
		this.parentTaskName = parentTask;
	}

	public String getName() {
		return name;
	}

	public List<EMLabel> getLabels() {
		return labels;
	}

	public void addLabel(EMLabel label) {
		getLabels().add(label);
	}

	public String getParentTaskName() {
		return parentTaskName;
	}

	public void setParentTaskName(String parentTaks) {
		this.parentTaskName = parentTaks;
	}

}
