package org.eclipse.app4mc.multicore.execution.logic.systemproxy.scheduler.core;

public class SchedulerEventChain {

	
	private String name;
	
	private String coreName;
	
	private String taskName;
	
	private String runnableName;
	
	
	public SchedulerEventChain(String name,  String runnableName, String taskName) {
		this.name = name;
		this.taskName = taskName;
		this.runnableName = runnableName;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the coreName
	 */
	public String getCoreName() {
		return coreName;
	}

	/**
	 * @param coreName the coreName to set
	 */
	public void setCoreName(String coreName) {
		this.coreName = coreName;
	}

	/**
	 * @return the taskName
	 */
	public String getTaskName() {
		return taskName;
	}

	/**
	 * @param taskName the taskName to set
	 */
	public void setTaskName(String taskName) {
		this.taskName = taskName;
	}

	/**
	 * @return the runnableName
	 */
	public String getRunnableName() {
		return runnableName;
	}

	/**
	 * @param runnableName the runnableName to set
	 */
	public void setRunnableName(String runnableName) {
		this.runnableName = runnableName;
	}

}
