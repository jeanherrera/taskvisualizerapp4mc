/**
 ********************************************************************************
 * Copyright (c) 2017 Dortmund University of Applied Sciences and Arts and others.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *    Dortmund University of Applied Sciences and Arts - initial API and implementation
 ********************************************************************************
 */

package org.eclipse.app4mc.multicore.execution.logic.systemproxy.multicoresystem;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.app4mc.multicore.execution.logic.executionmodel.ExecutionModel;
import org.eclipse.app4mc.multicore.execution.logic.executionmodel.types.EMCore;
import org.eclipse.app4mc.multicore.execution.logic.executionmodel.types.EMEventChainHist;
import org.eclipse.app4mc.multicore.execution.logic.executionmodel.types.EMEventChainHist.EMEventChainState;
import org.eclipse.app4mc.multicore.execution.logic.executionmodel.types.EMLabel;
import org.eclipse.app4mc.multicore.execution.logic.executionmodel.types.EMRunnable;
import org.eclipse.app4mc.multicore.execution.logic.executionmodel.types.EMTask;
import org.eclipse.app4mc.multicore.execution.logic.executionmodel.types.EMTaskEvent;
import org.eclipse.app4mc.multicore.execution.logic.executionmodel.types.EMTaskHistState;
import org.eclipse.app4mc.multicore.execution.logic.executionmodel.types.EMTaskState;
import org.eclipse.app4mc.multicore.execution.logic.executionmodel.types.LabelAccess;
import org.eclipse.app4mc.multicore.execution.logic.executionmodel.types.EMTaskHistState.EMTaskHistStateFactory;
import org.eclipse.app4mc.multicore.execution.logic.openmapping.OMUtil;
import org.eclipse.app4mc.multicore.execution.logic.systemproxy.scheduler.ISchedulerEventListener;
import org.eclipse.app4mc.multicore.execution.logic.systemproxy.scheduler.core.SchedulerEventChain;
import org.eclipse.app4mc.multicore.execution.logic.systemproxy.scheduler.core.SchedulerLabel;

public class EMTracer implements ISchedulerEventListener {

	private ExecutionModel executionModel;

	private EMCore emCore;

	private Map<String, EMTaskHistState> currentHist = new HashMap<>();

	static private Map<String, EMEventChainHist> eventChainHist = new HashMap<>();

	public EMTracer(String coreName, ExecutionModel m) {
		this.executionModel = m;
		this.emCore = new EMCore(coreName);
		m.addCore(emCore);
	}

	@Override
	public void onTaskAdded(String taskName, long period) {
		executionModel.addTask(new EMTask(taskName, period));
	}

	@Override
	public void onRunnableAdded(String taskName, String runnableName, List<SchedulerLabel> wLabels,
			List<SchedulerLabel> rLabels) {

		EMRunnable runn = new EMRunnable(runnableName, taskName);
		for (SchedulerLabel schLabel : wLabels) {
			EMLabel wrLb = new EMLabel(schLabel.getName(), LabelAccess.WRITE);
			runn.addLabel(wrLb);
		}

		for (SchedulerLabel schLabel : rLabels) {
			EMLabel rdLb = new EMLabel(schLabel.getName(), LabelAccess.READ);
			runn.addLabel(rdLb);
		}

		/*
		 * System.out.println("Task: " + taskName); System.out.println("Runnable: " +
		 * runn.getName()); for (EMLabel l : runn.getLabels()) {
		 * System.out.println(l.getName() + " " + l.getTypeOfAccess()); }
		 */

		executionModel.addRunnable(taskName, runn);

	}

	@Override
	public void onStartTask(String task, long time) {
		EMTaskHistState leaving = currentHist.get(task);
		if (leaving != null) {
			leaving.setEnd(time);
			executionModel.addTaskHistEntry(task, leaving);
		}

		EMTaskHistState currentRunning = EMTaskHistStateFactory.create(EMTaskState.RUNNING, emCore);
		currentRunning.setStart(time);
		currentHist.put(task, currentRunning);
		// executionModel.addTaskHistEntry(task, currentRunning);
	}

	@Override
	public void onTerminateTask(String task, long time) {
		EMTaskHistState leavingRunning = currentHist.get(task);
		if (leavingRunning != null) {
			leavingRunning.setEnd(time);
			executionModel.addTaskHistEntry(task, leavingRunning);
		}

		EMTaskHistState suspendedTask = EMTaskHistStateFactory.create(EMTaskState.SUSPENDED, emCore);
		suspendedTask.setStart(time);
		suspendedTask.setEvent(EMTaskEvent.TERMINATE);
		currentHist.put(task, suspendedTask);
	}

	@Override
	public void onStartChain(String eventChainName, String runnableName, String taskName,
			long time, long exTimeExpected) {
		EMEventChainHist startChain = new EMEventChainHist(eventChainName);
		startChain.setStart(time);
		startChain.setStartTask(taskName);
		startChain.setStartCore(emCore);
		startChain.setWcet(exTimeExpected);

		// add expected execution time of the task added
		/*System.out.println("Start Chain");
		System.out.println(eventChainName);
		System.out.println(taskName);
		System.out.println(time);
		System.out.println(emCore.getName());
		System.out.println(exTimeExpected);
		System.out.println("");*/

		eventChainHist.put(eventChainName, startChain);
	}

	@Override
	public void onEndChain(String eventChainName, String runnableName, String taskName,
			long time, long wcet) {

		/*
		 * System.out.println("----------"); System.out.println("Event Chain");
		 * System.out.println(eventChainName); System.out.println("Task Name");
		 * System.out.println(taskName); System.out.println("----Time------");
		 * System.out.println(time); System.out.println("");
		 */
		// Why getting null with WR_Label_4258????
		EMEventChainHist endingChain = eventChainHist.get(eventChainName);

		/*
		 * if (endingChain != null) {
		 * 
		 * System.out.println("get Start Task");
		 * System.out.println(endingChain.getStartTask());
		 * System.out.println("Current Task"); System.out.println(taskName); }
		 */
		if (endingChain != null && !endingChain.getStartTask().equalsIgnoreCase(taskName)
				&& endingChain.getStart() < time) { 
			endingChain.setEnd(time);
			endingChain.setEndCore(emCore);
			endingChain.setEndTask(taskName);

			/*
			 * System.out.println("End Chain"); System.out.println(eventChainName);
			 * System.out.println(taskName); System.out.println(time);
			 * System.out.println(emCore.getName());
			 */

			System.out.println(eventChainName);
			System.out.println(endingChain.getStartTask() + "->" + endingChain.getEndTask());
			System.out.println(endingChain.getStart() + "->" + endingChain.getEnd());
			System.out.println(endingChain.getStartCore().getName() + "->" + endingChain.getEndCore().getName());
			System.out.println(time+wcet);
			System.out.println("");
			// executionModel.addChainEvent();
		}
	}

	@Override
	public void onPreemptTask(String task, long time) {
		EMTaskHistState leavingRunning = currentHist.get(task);
		if (leavingRunning != null) {
			leavingRunning.setEnd(time);
			executionModel.addTaskHistEntry(task, leavingRunning);
		}

		EMTaskHistState readyTask = EMTaskHistStateFactory.create(EMTaskState.READY, emCore);
		readyTask.setStart(time);
		readyTask.setEvent(EMTaskEvent.PREEMPT);
		currentHist.put(task, readyTask);
	}

	@Override
	public void onStartIdleCore(long time) {
	}

	@Override
	public void onStopIdleCore(long time) {
	}

	@Override
	public void onActivateTask(String task, long time) {
		EMTaskHistState leavingSuspend = currentHist.get(task);
		if (leavingSuspend != null) {
			leavingSuspend.setEnd(time);
			executionModel.addTaskHistEntry(task, leavingSuspend);
		}

		EMTaskHistState readyTask = EMTaskHistStateFactory.create(EMTaskState.READY, emCore);
		readyTask.setStart(time);
		readyTask.setEvent(EMTaskEvent.ACTIVATE);
		currentHist.put(task, readyTask);
	}

	@Override
	public void onWaitTask(String task, long time, String muxName, String holder) {
		EMTaskHistState leavingRunning = currentHist.get(task);
		if (leavingRunning != null) {
			leavingRunning.setEnd(time);
			executionModel.addTaskHistEntry(task, leavingRunning);
		}

		EMTaskHistState waitingTask = EMTaskHistStateFactory.create(EMTaskState.WAITING, emCore);
		waitingTask.setStart(time);
		waitingTask.setEventCause(muxName);
		waitingTask.setNote(holder);
		currentHist.put(task, waitingTask);
	}

	@Override
	public void onReleaseTask(String task, long time) {
		EMTaskHistState leavingWaiting = currentHist.get(task);
		if (leavingWaiting != null) {
			leavingWaiting.setEnd(time);
			executionModel.addTaskHistEntry(task, leavingWaiting);
		}

		EMTaskHistState readyTask = EMTaskHistStateFactory.create(EMTaskState.READY, emCore);
		readyTask.setStart(time);
		readyTask.setEvent(EMTaskEvent.RELEASE);
		currentHist.put(task, readyTask);
	}

	@Override
	public void onTaskMissedDeadline(String task, long time, long remainingExectime) {
		EMTaskHistState before = currentHist.get(task);
		if (before != null) {
			before.setEnd(time);
			executionModel.addTaskHistEntry(task, before);
			currentHist.remove(task); // next will be activation
		}
		executionModel.addTaskDeadlineMissedEntry(task, emCore.getName(), time);
	}

	@Override
	public String getCoreName() {
		return emCore.getName();
	}

}
