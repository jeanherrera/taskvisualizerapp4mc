package org.eclipse.app4mc.multicore.execution.logic.systemproxy.scheduler.core;

public class SchedulerLabel {

	private String name;

	public SchedulerLabel(String name) {
		this.setName(name);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
