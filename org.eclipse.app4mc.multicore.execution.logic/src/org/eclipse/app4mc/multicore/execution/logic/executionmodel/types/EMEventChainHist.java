package org.eclipse.app4mc.multicore.execution.logic.executionmodel.types;

import java.util.Set;

import org.eclipse.app4mc.multicore.execution.logic.executionmodel.types.EMEventChainHist.TaskTime;

import javafx.scene.layout.VBox;

public class EMEventChainHist {

	public static final long UNKNOWN_PAST = Long.MIN_VALUE;
	public static final long UNKNOWN_FUTURE = Long.MAX_VALUE;

	private static final String UNKNOWN_STRING = "unknown";

	public enum EMEventChainState {
		START, END
	}

	public class TaskTime {
		private long start, end;

		public TaskTime(long start, long end) {
			this.start = start;
			this.end = end;
		}

		/**
		 * @return the start
		 */
		public long getStart() {
			return start;
		}

		/**
		 * @param start
		 *            the start to set
		 */
		public void setStart(long start) {
			this.start = start;
		}

		/**
		 * @return the end
		 */
		public long getEnd() {
			return end;
		}

		/**
		 * @param end
		 *            the end to set
		 */
		public void setEnd(long end) {
			this.end = end;
		}
	}

	private EMCore startCore;

	private EMCore endCore;

	private String eventChainName;

	private String runnable;

	private EMEventChainState eventState;

	// Strings for storing the tasks of the event chain.
	private String startTask = UNKNOWN_STRING;

	private String endTask = UNKNOWN_STRING;

	private EMTask startEMTask;

	private EMTask endEMTask;

	private EMTaskHistState startTaskHist;

	private EMTaskHistState endTaskHist;

	private TaskTime startTaskTimes = new TaskTime(0, 0);

	private TaskTime endTaskTimes = new TaskTime(0, 0);;

	private long startTaskStartTime = UNKNOWN_PAST;

	private long startTaskEndTime = UNKNOWN_FUTURE;

	private long endTaskStartTime = UNKNOWN_PAST;

	private long endTaskEndTime = UNKNOWN_FUTURE;

	/** Time at event occurrence */
	private long start = UNKNOWN_PAST;
	/** Time the */
	private long end = UNKNOWN_FUTURE;

	private long wcet;

	public EMEventChainHist(String eventChainName) {
		this.setEventState(eventState);
		this.setEventChainName(eventChainName);
	}

	/**
	 * @return the eventChainName
	 */
	public String getEventChainName() {
		return eventChainName;
	}

	/**
	 * @param eventChainName
	 *            the eventChainName to set
	 */
	public void setEventChainName(String eventChainName) {
		this.eventChainName = eventChainName;
	}

	/**
	 * @return the eventState
	 */
	public EMEventChainState getEventState() {
		return eventState;
	}

	/**
	 * @param eventState
	 *            the eventState to set
	 */
	public void setEventState(EMEventChainState eventState) {
		this.eventState = eventState;
	}

	/**
	 * @return the runnable
	 */
	public String getRunnable() {
		return runnable;
	}

	/**
	 * @param runnable
	 *            the runnable to set
	 */
	public void setRunnable(String runnable) {
		this.runnable = runnable;
	}

	/**
	 * @return the startTask
	 */
	public String getStartTask() {
		return startTask;
	}

	/**
	 * @param startTask
	 *            the startTask to set
	 */
	public void setStartTask(String startTask) {
		this.startTask = startTask;
	}

	/**
	 * @return the endTask
	 */
	public String getEndTask() {
		return endTask;
	}

	/**
	 * @param endTask
	 *            the endTask to set
	 */
	public void setEndTask(String endTask) {
		this.endTask = endTask;
	}

	/**
	 * @return the start
	 */
	public long getStart() {
		return start;
	}

	/**
	 * @param start
	 *            the start to set
	 */
	public void setStart(long start) {
		this.start = start;
	}

	/**
	 * @return the end
	 */
	public long getEnd() {
		return end;
	}

	/**
	 * @param end
	 *            the end to set
	 */
	public void setEnd(long end) {
		this.end = end;
	}

	/**
	 * @return the endCore
	 */
	public EMCore getEndCore() {
		return endCore;
	}

	/**
	 * @param endCore
	 *            the endCore to set
	 */
	public void setEndCore(EMCore endCore) {
		this.endCore = endCore;
	}

	/**
	 * @return the startCore
	 */
	public EMCore getStartCore() {
		return startCore;
	}

	/**
	 * @param startCore
	 *            the startCore to set
	 */
	public void setStartCore(EMCore startCore) {
		this.startCore = startCore;
	}

	/**
	 * @return the wcet
	 */
	public long getWcet() {
		return wcet;
	}

	/**
	 * @param wcet
	 *            the wcet to set
	 */
	public void setWcet(long wcet) {
		this.wcet = wcet;
	}

	/**
	 * @return the endEMTask
	 */
	public EMTask getEndEMTask() {
		return endEMTask;
	}

	/**
	 * @param endEMTask
	 *            the endEMTask to set
	 */
	public void setEndEMTask(EMTask endEMTask) {
		this.endEMTask = endEMTask;
	}

	/**
	 * @return the startEMTask
	 */
	public EMTask getStartEMTask() {
		return startEMTask;
	}

	/**
	 * @param startEMTask
	 *            the startEMTask to set
	 */
	public void setStartEMTask(EMTask startEMTask) {
		this.startEMTask = startEMTask;
	}

	/**
	 * @return the startTaskHist
	 */
	public EMTaskHistState getStartTaskHist() {
		return startTaskHist;
	}

	/**
	 * @param startTaskHist
	 *            the startTaskHist to set
	 */
	public void setStartTaskHist(EMTaskHistState startTaskHist) {
		this.startTaskHist = startTaskHist;
	}

	/**
	 * @return the endTaskHist
	 */
	public EMTaskHistState getEndTaskHist() {
		return endTaskHist;
	}

	/**
	 * @param endTaskHist
	 *            the endTaskHist to set
	 */
	public void setEndTaskHist(EMTaskHistState endTaskHist) {
		this.endTaskHist = endTaskHist;
	}

	/**
	 * @return the startTaskTimes
	 */
	public TaskTime getStartTaskTimes() {
		return startTaskTimes;
	}

	/**
	 * @param startTaskTimes
	 *            the startTaskTimes to set
	 */
	public void setStartTaskTimes(long start, long end) {
		setStartTaskStartTime(start);
		setStartTaskEndTime(end);
	}

	/**
	 * @return the endTaskTimes
	 */
	public TaskTime getEndTaskTimes() {
		return endTaskTimes;
	}

	/**
	 * @param endTaskTimes
	 *            the endTaskTimes to set
	 */
	public void setEndTaskTimes(long start, long end) {
		setEndTaskStartTime(start);
		setEndTaskEndTime(end);
	}

	/**
	 * @return the startTaskStartTime
	 */
	public long getStartTaskStartTime() {
		return startTaskStartTime;
	}

	/**
	 * @param startTaskStartTime the startTaskStartTime to set
	 */
	public void setStartTaskStartTime(long startTaskStartTime) {
		this.startTaskStartTime = startTaskStartTime;
	}

	/**
	 * @return the startTaskEndTime
	 */
	public long getStartTaskEndTime() {
		return startTaskEndTime;
	}

	/**
	 * @param startTaskEndTime the startTaskEndTime to set
	 */
	public void setStartTaskEndTime(long startTaskEndTime) {
		this.startTaskEndTime = startTaskEndTime;
	}

	/**
	 * @return the endTaskStartTime
	 */
	public long getEndTaskStartTime() {
		return endTaskStartTime;
	}

	/**
	 * @param endTaskStartTime the endTaskStartTime to set
	 */
	public void setEndTaskStartTime(long endTaskStartTime) {
		this.endTaskStartTime = endTaskStartTime;
	}

	/**
	 * @return the endTaskEndTime
	 */
	public long getEndTaskEndTime() {
		return endTaskEndTime;
	}

	/**
	 * @param endTaskEndTime the endTaskEndTime to set
	 */
	public void setEndTaskEndTime(long endTaskEndTime) {
		this.endTaskEndTime = endTaskEndTime;
	}

}
